﻿Shader "Hidden/SSS_Scat"
{
	Properties
	{
		_Color("Main Color", Float) = 1.0
	}

	SubShader
	{
		//this subShader is same with "Unlit/Color" shader, except the RenderType change to "GroupBloom"
		Tags{ "RenderType" = "SSS" }
		LOD 100
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata_t 
		{
			float4 vertex : POSITION;
		};
		struct v2f {
			float4 vertex : SV_POSITION;
			UNITY_FOG_COORDS(0)
		};


		float _ScatterRadius;

		v2f vert(appdata_t v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			UNITY_TRANSFER_FOG(o,o.vertex);
			return o;
		}

		fixed4 frag(v2f i) : COLOR
		{
			// 这里怎么记录深度啊
			// 这里本来应该只需要一个维度记的 但是我不会写……
			fixed4 col = float4(0,0,0,1); 
			col.rgb = _ScatterRadius;
			//// 这两个是干嘛的啊！
			//UNITY_APPLY_FOG(i.fogCoord, col);
			//UNITY_OPAQUE_ALPHA(col.a);
			return col;
		}
			ENDCG
		}
	}

	SubShader
	{
		//because this subShader renders pure black, so we need not support fog
		Tags{ "RenderType" = "Opaque" }
		LOD 100

		Pass
		{

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata_t 
			{
				float4 vertex : POSITION;
			};

			struct v2f 
			{
				float4 vertex : SV_POSITION;
			};

			fixed4 _SubsurfaceColor;

			v2f vert(appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}

			fixed4 frag(v2f i) : COLOR
			{
				fixed4 col = float4(0,0,0,1);
				return col;
			}
			ENDCG
		}
	}
}