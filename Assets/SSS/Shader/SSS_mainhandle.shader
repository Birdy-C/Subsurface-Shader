﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/SSS_mainhandle" {
	Properties
	{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_SubTex("Subsurface (RGB)", 2D) = "white" {}
		_BlurTex("Blur (REB)", 2D) = "white" {}

		//_BloomFactor("Bloom Factor",Range(0,10)) = 0.1
	}
	SubShader
	{
		ZWrite Off //注意，这句和下一句对于iOS特别重要，如果没有这两句，在iOS真机上运行将会黑屏。
		ZTest Always
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			uniform sampler2D _MainTex;
			uniform sampler2D _SubTex;
			uniform sampler2D _BlurTex;

			float _BloomFactor;
			v2f_img vert(appdata_img v) : POSITION
			{
				v2f_img o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord.xy;
				return o;
			}
			fixed4 frag(v2f_img i) :COLOR
			{
				//Get the colors from the RenderTexture and the uv's
				//from the v2f_img struct
				fixed4 mainColor = tex2D(_MainTex, i.uv);
				fixed4 subColor = tex2D(_SubTex, i.uv);
				fixed4 blurColor = tex2D(_BlurTex, i.uv);

				// 这句是后的一步
				// F(Col * Subsurface Col  *Falloff Col)+ Col * (1 - Subsurface Col)
				// = BlurColor + Col * (1 - Subsurface Col)
				fixed4 finalColor = blurColor;
				finalColor.rgb += mainColor * (1 - subColor);

				//fixed4 col = float4(1, 1, 1, 1);
				//col.rgb = mainColor * (1 - subColor);
			
				return finalColor;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}