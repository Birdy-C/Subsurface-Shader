﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SSS_Setting : MonoBehaviour {
    [Range(0.01f, 1.0f)]
    public float SSS_Scale = 0.5f;
    [Range(1, 7)]
    public int SSS_SampleSet = 7;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int downSample()
    {
        return SSS_SampleSet;
    }

    public float Scale()
    {
        return SSS_Scale;
    }

}
