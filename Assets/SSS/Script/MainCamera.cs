﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour {

    public Material SSS_mainhandle;
    void OnRenderImage(RenderTexture sourceTexture, RenderTexture destTexture)
    {
        //Debug.Log (sourceTexture);
        //Copies source texture into destination render texture with a shader.
        Graphics.Blit(sourceTexture, destTexture, SSS_mainhandle);
    }
}
