﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class SubCamera : MonoBehaviour
{
    public Camera m_mainCameraRef;
    public Shader SubRenderShader;
    // Use this for initialization
    void Start()
    {
        
        GetComponent<Camera>().enabled = false;//it is equals to uncheck Camera component in Inspector
        //synchronizePosAndRotWithMainCamera();
        //synchronizeProjModeAndFrustomWithMainCamera();
    }
    void LateUpdate()
    {
        // 设置子物体之后应该不需要一直同步位置
        //synchronizePosAndRotWithMainCamera();
        //Rendering with Replaced Shaders: http://www.cnblogs.com/wantnon/p/4528677.html
        GetComponent<Camera>().RenderWithShader(SubRenderShader, "RenderType");
    }
    void synchronizePosAndRotWithMainCamera()
    {
        transform.position = m_mainCameraRef.transform.position;
        transform.rotation = m_mainCameraRef.transform.rotation;
    }
    void synchronizeProjModeAndFrustomWithMainCamera()
    {
        GetComponent<Camera>().orthographic = m_mainCameraRef.orthographic;
        GetComponent<Camera>().orthographicSize = m_mainCameraRef.orthographicSize;
        GetComponent<Camera>().nearClipPlane = m_mainCameraRef.nearClipPlane;
        GetComponent<Camera>().farClipPlane = m_mainCameraRef.farClipPlane;
        GetComponent<Camera>().fieldOfView = m_mainCameraRef.fieldOfView;
    }

}