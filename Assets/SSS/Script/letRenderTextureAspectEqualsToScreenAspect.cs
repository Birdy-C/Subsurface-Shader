﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class letRenderTextureAspectEqualsToScreenAspect : MonoBehaviour
{
    public List<RenderTexture> m_rt;
    // Use this for initialization
    void Start()
    {
        //float screenAspect = (float)(Screen.width) / Screen.height;
        ////Debug.Log (screenAspect);
        foreach (RenderTexture rt in m_rt)
        {
            rt.height = Screen.height;
            rt.width = Screen.width;
            //rt.width = (int)(rt.height * screenAspect);
        }
    }
}