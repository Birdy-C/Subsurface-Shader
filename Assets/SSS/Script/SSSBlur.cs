﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SSSBlur : PostEffectBase
{


    public GameObject MainCamera;

    int samplerScale = 1;

    private Camera _mainCam = null;
    public Camera MainCam
    {
        get
        {
            if (_mainCam == null)
                _mainCam = GetComponent<Camera>();
            return _mainCam;
        }
    }

    void OnEnable()
    {
        //maincam的depthTextureMode是通过位运算开启与关闭的
        MainCam.depthTextureMode |= DepthTextureMode.Depth;
    }

    void OnDisable()
    {
        MainCam.depthTextureMode &= ~DepthTextureMode.Depth;
    }

    // Use this for initialization
    // 这个函数用来做模糊
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (_Material)
        {

            int downSample = MainCamera.GetComponent<SSS_Setting>().downSample();
            
            RenderTexture tempX = RenderTexture.GetTemporary(source.width >> downSample, source.height >> downSample, 0, source.format);
            RenderTexture tempY = RenderTexture.GetTemporary(source.width >> downSample, source.height >> downSample, 0, source.format);
            RenderTexture temp = RenderTexture.GetTemporary(source.width >> downSample, source.height >> downSample, 0, source.format);

            //直接将场景图拷贝到低分辨率的RT上达到降分辨率的效果
            //Graphics.Blit(source, temp1);

            ////高斯模糊，两次模糊，横向纵向，使用pass0进行高斯模糊

            // 我多糊一次吧感觉一次看不太出来（捂脸）

            // 这个是竖方向
            _Material.SetVector("_offsets", new Vector4(0, samplerScale, 0, 0));

            Graphics.Blit(source, temp, _Material, 0);
            Graphics.Blit(temp, tempY, _Material, 0);

            // 这个是横方向
            _Material.SetVector("_offsets", new Vector4(samplerScale, 0, 0, 0));
            Graphics.Blit(source, temp, _Material, 0);
            Graphics.Blit(temp, tempX, _Material, 0);

            //景深操作，景深需要两的模糊效果图我们通过_BlurTex变量传入shader
            _Material.SetTexture("_BlurTexX", tempX);
            _Material.SetTexture("_BlurTexY", tempY);


            //设置shader的参数，主要是焦点和远近模糊的权重，权重可以控制插值时使用模糊图片的权重

            _Material.SetFloat("alpha", MainCamera.GetComponent<SSS_Setting>().Scale());

            //使用pass1进行景深效果计算，清晰场景图直接从source输入到shader的_MainTex中
            Graphics.Blit(source, destination, _Material, 2);

            //释放申请的RT
            RenderTexture.ReleaseTemporary(tempX);
            RenderTexture.ReleaseTemporary(tempY);
        }
        else
        {
            Debug.Log("NO RENDER");
        }
    }

    //计算设置的焦点被转换到01空间中的距离，以便shader中通过这个01空间的焦点距离与depth比较
    private float FocalDistance01(float distance)
    {
        return MainCam.WorldToViewportPoint((distance - MainCam.nearClipPlane) * MainCam.transform.forward + MainCam.transform.position).z / (MainCam.farClipPlane - MainCam.nearClipPlane);
    }
}
