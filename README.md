尝试把UE4里面的人像提取出来（因为无法直接获得UE4使用的shader代码），这个文章是基于UE4使用的sss。此外还有其他的sss呈现方式。
原作连接https://docs.unrealengine.com/en-us/Engine/Rendering/Materials/LightingModels/SubSurfaceProfile
（我突然发现这个人像又没有头发又没有眼睛哈哈哈哈哈省了很多工作量呢）

最终结果，在网上随便找了一个人脸的model
![result](pic/face.png)

# 算法分析
UE4的文档中指出：
1. 这个shader基于屏幕空间上而不是纹理空间【第一段】
2. 对于非镜面反射，使用一种类似于高斯模糊的双通滤波方式，最后再将镜面和非镜面部分叠加（镜面部分会受到位置的影响）【Technical Details】
4. 一个实现上的优化：把镜面和非镜面光照（view-dependent and non-view-dependent lighting）分别存储在64bit的高低32bit上。【Technical Details】

接下来结合论文
http://www.iryoku.com/sssss/downloads/Screen-Space-Perceptual-Rendering-of-Human-Skin.pdf

论文提出的关于sss的前提：
![前提](https://img-blog.csdn.net/20180911101010831?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JpcmR5Xw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
由此得出了一个不知道怎么得出来的满足前提的公式（这不是在建模的我吗）
![这里写图片描述](https://img-blog.csdn.net/20180911101212819?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2JpcmR5Xw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
$\alpha$和$\beta$的值需要最后重新确认。作者取出的值大概在11和800.
所以觉得很好的那些效果其实实现的核心只是一个滤波/模糊（没想到吧）

最后论文作者邀请了一堆人来目测这个结果和之前的结果哪个更像人（……）

## Main Parameter
接下来看看UE4中sss相关的参数。

| Property Name        | Description                                                  | 猜测影响|
| -------------------- | ------------------------------------------------------------ |
|**r.SSS.Scale**| Can be used to scale the effect for quick experiments. Setting this to 0 will disable the effect. Setting numbers higher than 0 will increase the effect that can be seen in the image sequence below.| 对应$\alpha$|
|**r.SSS.SampleSet**|Sets the number of samples used. Decreasing this will cause the effect to run faster. However, this will mean that the effect will have a lower quality, and rendering artifacts could show up.|影响滤波采样率（后来写的过程中感觉应该是用户自定Render Texture的采样率）|
| **Scatter Radius**   | The distance in world space units to perform the scatter.    |对应$\beta$|
| **Subsurface Color** | The Subsurface Color can be used as a weight for the Subsurface effect. Black means there is no Subsurface scattering. White means all lighting is entering the material and gets scattered around. A non-grayscale value gives more control over what color contributions are entering the surface, resulting in a more complex looking shading. |F(Col*Subsurface Col*Falloff Col)+Col*(1-Subsurface Col)|
| **Falloff Color**    | The Falloff Color defines the material scattering color once the light has entered the material. You should avoid using a vivid color here if you want to get a more complex shading variation over the area where you see the scattering. |F(Col*Subsurface Col*Falloff Col)+Col*(1-Subsurface Col)|

前面两个处理的是shader的总的参数，后面三个处理的是单个物体的参数。
解释一下最后两个公式。虽然是我脑补的……
根据sss的特点，照射到物体的光线一部分直接正常的反射，另一部分由于物体性质进入内部也就是我们要做的sss。所以分为两部分。
接下来考虑到进入内部的衰减，也就是Falloff Color。猜测这个颜色如果过于明亮权值会偏大，整个图的结果会比较模糊。
F(）是指对这一部分进行滤波。

